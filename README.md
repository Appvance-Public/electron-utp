# AppvanceUTP Server Installer

This is a standalone installer for AppvanceUTP. This bundles JRE, Mysql, And Appvance server with an electron interface for install process to install the Appvance server. This will also create an icon for Appvance that can be clicked on to bring up a windows to appvance or to exit appvance.


### Installation
##### Prerequites:
 - Have node/npm installed.
 - Download the version of Appvance you want the installer to use.
 - Download the latest version of standalone community edition MYSQL to use (This will be different per Win vs MacOS).

#### Install Steps
1. Clone the repo
2. run `npm install`
3. Update root level package.json to change the extraResources to specify the appropriate file location for appvanceUTP and MYSQL 
4. run `npm run dist` to build the package

### Tech
Electron provide the GUI during the install process. Electron-pacakger is the tool which handle the full bundle of the installation into an app/exe. The installation will install into it's own space so is self contained. For Mac, this means within the .app bundle. For windows, this is a resources folder relative to the application.