'use strict';
const electron = require('electron');
// const path = require('path');
// var exec = require('child_process').exec;
var async = require('async');
var request = require('request');
// var os = require('os');
var fs = require('fs');
// var mysql = require('mysql');
// var utpApi = require('./utpApi');
var log = require('electron-log');
var config = require('./config');
var jszip = require('jszip');
log.transports.file.level = 'warning';

var dialog = electron.dialog;
var utils = {};
utils.download_file = function(url, path, callback) {
  request({
      uri: url
    })
    .pipe(fs.createWriteStream(path))
    .on('error', function(err) {
      callback(err);
    })
    .on('close', function() {
      callback();
    });
};

utils.mac_unzip = function(inpath, outpath, callback) {
  var targz = require('targz');
  targz.decompress({
    src: inpath,
    dest: outpath
  }, function(err) {
    if (err) {
      console.log(err);
      callback(err);
    } else {
      log.info("Extracted file from " + inpath + " to " + outpath);
      callback(null);
    }
  });
};

utils.win_unzip = function(inpath, outpath, callback) {
  var unzip = require('unzip');
  fs.createReadStream(inpath).pipe(unzip.Extract({
      path: outpath
    }))
    .on('error', function(err) {
      callback(err);
    })
    .on('close', function() {
      callback(null);
    });
};

utils.onceAppvanceUp = function(waitUntilRegistered, callback) {
  var ready = false;
  async.until(function() {
    return ready;
  }, function(callback) {
    setTimeout(function() {
        if (config.mysqlAppvanceInitialized) {
          request(config.appvance.getUrl(), {
            timeout: 3000
          }, function(error, response) {
            global.windows[0].webContents.send('current-proc', 'checking main appvance process');
            if (error) {
              if (['ETIMEDOUT', 'ECONNREFUSED', 'ESOCKETTIMEDOUT'].includes(error.code)) {
                callback(null);
              } else {
                console.log(error);
                callback(error);
              }
            } else if (response.statusCode == 200) {
              if (waitUntilRegistered) {
                if (config.registered) {
                  ready = true; // Show the HTML for the Google homepage. 
                  log.info("Appvance is up and registered");
                }
              } else {
                ready = true;
                log.info("Appvance is up, ignore registered");
              }
              callback(null);
            } else {
              // log.debug("Appvance is down");
              callback(null);
            }
          });
        } else {
          callback(null);
        }
      },
      1000);
  }, function(err) {
    if (err) {
      callback(err);
    } else {
      callback(null);
    }
  });
};

utils.downloadLogs = function(){
  var zip = new jszip();
  // var zipfolder = jszip.folder("logs");
  var files = [ 
    log.transports.file.file,
    config.appvanceCommandOutputPath,
    config.appvanceCommandErrorPath ,
    config.mysqlCommandOutputPath,
    config.mysqlCommandErrorPath 
  ];
  async.each(files, function(file, callback){
      fs.readFile(file, function(err, data){
        if(err){
          log.error("Failed to read file "+file);
          if (err.code === "ENOENT"){
            callback(null);
          } else{
            callback(err);
          }
        } else{
          var fileName = file.split('/').pop();
          zip.folder("logs").file(fileName, data.toString());
          callback(null);
        }
      });
    }, function(err){
      if(err){
        log.error("Failed to create log zip file");
        log.error(err);
      } else{
        var options = {
          title:"Save Logs",
          defaultPath:"logs.zip"
        };
        dialog.showSaveDialog(options, function (fileName) {
          if (fileName === undefined){
               log.error("You didn't save the file");
               return;
          }
          // fileName is a string that contains the path and filename created in the save file dialog.  
          zip
            .generateNodeStream({type:'nodebuffer',streamFiles:true})
            .pipe(fs.createWriteStream(fileName))
            .on('finish', function () {
              // JSZip generates a readable stream with a "end" event,
              // but is piped here in a writable stream which emits a "finish" event.
              log.warn(fileName+" written.");
            });          
        });
      }
    }
  );
};
module.exports = utils;
