"use strict";
// TODO: SetUp auto installer
// if(require('electron-squirrel-startup')) return;
// const autoUpdater = require('auto-updater');  

// autoUpdater.addListener("update-available", function(event) {  
// });
// autoUpdater.addListener("update-downloaded", function(event, releaseNotes, releaseName, releaseDate, updateURL) {  
// });
// autoUpdater.addListener("error", function(error) {  
// });
// autoUpdater.addListener("checking-for-update", function(event) {  
// });
// autoUpdater.addListener("update-not-available", function(event) {  
// });
// const os = require('os');  
// const feedURL = 'http://appvance.s3.amazonaws.com/updates/latest/win' + (os.arch() === 'x64' ? '64' : '32');  
// autoUpdater.setFeedURL(feedURL); 

const electron = require('electron');
const path = require('path');
var exec = require('child_process').exec;
var spawn = require('child_process').spawn;
var async = require('async');
var fs = require('fs');
var mysql = require('mysql');
var log = require('electron-log');

var logLevel = 'warn';
if(process.env.NODE_ENV === 'DEVELOPMENT'){
  logLevel = 'debug';
}
log.transports.file.level = logLevel;
log.transports.console.level = logLevel;

// Determine os specifics for path. Mac, it is within the app bundle. Windows,
// it is in the same directory of the executable.
var baseFilePath = '';
var javaExecutable = '';

if (process.platform == 'win32') {
  javaExecutable = 'java.exe';
  baseFilePath = path.normalize('resources');
} else if (process.platform == 'darwin') {
  javaExecutable = 'java';
  baseFilePath = path.resolve(path.join(__dirname, '../'));
} else {
  throw new Error("cannot determine os " + process.platform);
}

// console.log(baseFilePath);
// console.log(path.normalize(path.join(baseFilePath, 'appvance.log')));

// The file location has to be set before calling the log function.
log.transports.file.file = path.normalize(path.join(baseFilePath, 'appvance.log'));

//Import customer modules once logging has been established
var utpApi = require('./utpApi');
var config = require('./config');
var utils = require('./utils');
var acmConnector = require('./acmConnector');


// Module to control application life.
const app = electron.app;
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow;
const Menu = electron.Menu;
const Tray = electron.Tray;

//global variable for db conenction.
var dbConnection;
//This variable used to handle expected errors because program is shuttind down.
var stoppingProgram = false;

/**
 * Callback loop to maintain connection the the local mysql server.
 */
function maintainDbConnection(){
  dbConnection = mysql.createConnection(config.getDbConfig());

  dbConnection.connect(function(err){
    if(err){
      if(!stoppingProgram){
        log.debug("Error when connecting to db: ",err.code);
        setTimeout(maintainDbConnection, 2000);
      }
    } else{
      log.warn("Connected to db.");
    }
  });
  dbConnection.on('error', function(err){
    if (['PROTOCOL_CONNECTION_LOST',"ECONNREFUSED", "ECONNRESET", "ETIMEDOUT"].includes(err.code)){
      if(!stoppingProgram){
        maintainDbConnection();
      }
    } else {
      log.warn("mysql disconnected");
      throw err;
    }
  });
}

/**
 * Send message to main window for event 'current-proc'. This is always sent to 
 * main window which may or may not have a listener for the event.
 * @param {string} message - message that would be displayed
 */
function sendInstallMessage(message){
  global.windows[0].webContents.send('current-proc',message);
}


// Keep a global reference of the window objects, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
global.windows = [];
var appvanceWindow;
var helpWindow;
var startUpBrowserWindow;
var setupBrowserWindow;
var advancedBrowserWindow;

// Maintain reference to mysql/appvance child for starting/stoping processes
var mysqlChild;
var appvanceChild;

/**
 * Installs Mysql, setting the mysqlInitialized config valu, using the bundled 
 * mysql if not already initialized. Refer to getMysqlInitCommand in config.js 
 * for specific command line arguements.
 * @param {Callback} callback - callback to be executed after installing mysql. returns an error if occured, otherwise null
 */
function installMysql(callback){
  if (config.mysqlInitialized) {
    log.info("mysql already initialized");
    //Send install progress info to main window
    sendInstallMessage('database already initialized');
    callback(null);
  } else {
    log.info("mysql not installed. Starting instalation");
    log.debug("Starting mysql install. command: "+config.getMysqlInitCommand());
    var child = exec(config.getMysqlInitCommand(), {}, function(err, stdout, stderr) {
      if (err) {
        log.error("Unable to initialize mysql");
        log.error("command: " + config.getMysqlInitCommand());
        log.error('stdout: ' + stdout);
        log.error('stderr: ' + stderr);
        callback(err);
      } else {
        config.mysqlInitialized = true;
        config.saveSystemSettings();
        log.info("Initialized/installed mysql");
        log.debug("stdout: " + stdout);
        callback(null);
      }
    });
    log.debug("Started mysql installation");
    sendInstallMessage("initializing database");
    child.stdout.on('data', function(data) {
      sendInstallMessage(data.toString());
    });
  }
}

/**
* This will wait to run a callback until the mysql server is up and accepting
* connections. 
* @param {Callback} callback - callback to be executed once mysqlUp.  
*/
function ensureMysqlUp(callback){
  var ready = false;
  sendInstallMessage('waiting for database to be available');
  async.until(function() {
      return ready;
    },
    function(cb) {
      if (stoppingProgram){
        cb(new Error("stopping check, shutting down"));
      }
      setTimeout(function() {
        log.debug("testing mysql connection");
        dbConnection.query("SELECT true;", function(err) {
          if (err) {
            if (["ECONNREFUSED", "ECONNRESET"].includes(err.code)) {
              log.info("mysql still not up.");
              cb(null);
            } else {
              cb(null);
            }
          } else {
            ready = true;
            log.info("mysql is up and accepting connections");
            cb(null);
          }
        });
      }, 5000);
    },
    function(err) {
      if (err) {
        callback(err);
      } else {
        sendInstallMessage('database available');
        callback(null);
      }
    }
  );
}

/**
* Provision the mysql server by running the appvance init sql(to initialize 
* appvance user) and then the appvance sql(create tables and such).
* @param {Callback} callback - callback executed once provisioning completed.
*/
function provisionMysql(callback){
  ensureMysqlUp(function(){
    sendInstallMessage('starting database provisioning');
    async.series([
      function(cb) {
        //Read then execute the mysql init file.
        fs.readFile(config.mysqlInitSqlFile, function(err, data) {
          if (err) {
            cb(err);
          } else {
            log.debug("read init sql script");
            var init_sql = data.toString();
            dbConnection.query(init_sql, function(err) {
              if (err) {
                log.error("Failed running init sql");
                cb(err);
              } else {
                global.windows[0].webContents.send('current-proc', 'running init scripts');
                log.info("ran init sql script");
                cb(null);
              }
            });
          }
        });
      },
      function(cb) {
        // This had to be broken out into it's own command vs executing the sql 
        // directly. I don't remember why this had to be done at this point. 
        var child = exec(config.getMysqlAppvanceSqlCommand(), function(err){
          if (err) {
            log.error("Unable to run appvance provision sql");
            log.error(err);
            cb(err);
          } else {
            sendInstallMessage('provisioned database for appvance');
            log.info("appvance provision sql succesful");
            cb(null);
          }
          child.stdout.on('data', function(data) {
            sendInstallMessage(data.toString());
          });
        });
      }
    ], function(err) {
      if (err) {
        if (!stoppingProgram){
          log.error("failed to initialize mysql");
          log.error(err);
          callback(err);
        }
      } else {
        log.info("Provisioned mysql for appvance");
        sendInstallMessage('database fully provisioned');
        //Saving this so that on startup, we can check this and skip initialization.
        config.mysqlAppvanceInitialized = true;
        config.saveSystemSettings();
        callback(null);
      }
    });
  });
}

/**
* Start the Mysql Server.
*/
function startMysql() {
  log.info("Starting Mysql process.");
  global.windows[0].webContents.send('current-proc', 'verifying database');
  log.warn("Starting mysql");
  var options = {
    shell: true
  };
  mysqlChild = spawn(config.mysqlStartMainCommand, config.getMysqlStartMainCommandArgs(), options);
  mysqlChild.on('error', function(err){
    //Don't complain if the program is supposed to be stopping
    if (!stoppingProgram) {
      log.error("Mysql ran into an error");
      log.error("command: " + config.getMysqlStartCommand());
      throw err;
    }
  });
  mysqlChild.on('end', function(){
    if(!stoppingProgram){
      log.warn('mysql ended unexpectedly');
    }
  });
  global.windows[0].webContents.send('current-proc', 'starting database');

  mysqlChild.stdout.on('data', function(data) {
    //Write to log
    config.mysqlCommandStdoutWriteSteam.write(data.toString());
    sendInstallMessage(data.toString());
  });

  mysqlChild.stderr.on('data', function(data) {
    //write to log
    config.mysqlCommandStderrWriteSteam.write(data.toString());
  });
}

/**
* Start the Appvance Server
*/
function startAppvanceProc() {
  //Only start appvance if it isn't already running
  if (!appvanceChild) {
    var options = {
      cwd: config.appvanceStartDir,
      shell: true
    };
    appvanceChild = spawn(config.appvanceMainCommand, config.getAppvanceMainCommandArgs(), options);


    appvanceChild.on('error', function(err){
      if (!stoppingProgram) {
        log.error("Appvance service ran into an error");
        log.error("command: " + config.getAppvanceCommand());
        throw err;
      }
    });

    sendInstallMessage('starting main appvance service');

    appvanceChild.stdout.on('data', function(data) {
      //Write to log
      config.appvanceCommandStdoutWriteSteam.write(data.toString());
      sendInstallMessage('current-proc', data.toString());
    });

    appvanceChild.stderr.on('data', function(data) {
      //Write to log
      config.appvanceCommandStderrWriteSteam.write(data.toString());
    });
  }
}

/**
* Start appvance server and then open a window to the appvance main page.
*/
function startAppvanceWindow() {
  //Verify Appvance is running, then load it.
  startAppvanceProc();
  utils.onceAppvanceUp(true, function(err) {
    if (err) {
      if (!stoppingProgram) {
        log.error("failed to start appvance");
        throw err;
      }

    } else {

      sendInstallMessage('appvance process up, loading');
      log.warn("Appvance is up, serving " + config.appvance.getUrl());
      // nodeIntegration off since appvance server runs on normal webpage but
      // electron window is different since it runs as module. setting false
      // Allows the window operate like a normal chromium browser window
      var options = {
        width: 1400,
        height: 1000,
        webPreferences: {
          nodeIntegration: false
        },
        show: false
      };
      var appvanceWindow = new BrowserWindow(options);
      // Default electron behavior is to open all external links in a new window 
      // and since appvance server is hosted by java, all links are external...
      // This ensures that clicking on these links will keep it within electron window
      appvanceWindow.webContents.on('new-window', function(e, url) {
        log.debug("new url: "+url);
        e.preventDefault();
        appvanceWindow.loadURL(url);
      });
      // Check if link being clicked is help window 'userguide'. If so, open in a 
      // a new window slightly ajar.
      appvanceWindow.webContents.on('will-navigate', function(e, url){
        log.debug("new url: "+url);
        if(url.indexOf('userguide') > -1 ){
          var contentBounds = global.windows[0].getContentBounds();
          options.x = contentBounds.x + 25;
          options.y = contentBounds.y + 25;
          e.preventDefault();
          var helpWindow = new BrowserWindow(options);
          helpWindow.loadURL(url);
          helpWindow.on('ready-to-show', function(){
            helpWindow.show();
            helpWindow.focus();
          });
        }
      });

      //This actually loads appvance page.
      appvanceWindow.loadURL(config.appvance.getUrl());
      appvanceWindow.on('ready-to-show', function() {
        appvanceWindow.show();
        appvanceWindow.focus();
        global.windows[0] = appvanceWindow;
        appvanceWindow = null;
      });
      appvanceWindow.focus();

      appvanceWindow.on('closed', function() {
        appvanceWindow = null;
      });


      // Update appvance to expect mysql on this port. Generally this will be the 
      // 54321 port to avoid collisions with mysql already running on machine.
      utpApi.admin.loggin(config.appvance.host, config.appvance.user, config.appvance.password, function(err) {
        if (err) {
          log.error(err);
        } else {
          log.info("Setting result repository");
          var params = {
            "username" : "utp",
            "password" : "utp",
            "engine" : "MySQL",
            "url" : "jdbc:mysql://"+config.mysql.host+":"+config.mysql.port+"/utp",
            "database" : "utp",
            "maxActive" : "100",
            "maxIdle" : "30",
            "maxWait" : "1000",
            "abandonedTimeout" : "60",
            "autoCommit" : true,
            "logAvandoned" : true,
            "removeAvandoned" : true,
            "enabled" : true
          };
          utpApi.preferences.saveResultsRepository(config.appvance.host, params, function(err){
            if(err){
              log.error("Failed to set Result Repository");
              log.error(err);
            }else{
              log.warn("Updated result repository");
            }
          });
        }
      });
    }
  });
}

function enableDevTools(){
  global.windows[0].webContents.openDevTools();
}

/**
* Set the menu bar. This also enables copy/paste as electron requires they be 
* part of the menu bar to enable them.
*/
function setMenu(){
  var template = [];
  if (process.platform === 'darwin') {
    template.push({
      label: 'FromScratch',
      submenu: [{
        label: 'Quit',
        accelerator: 'CmdOrCtrl+Q',
        click: stopAllProcess
      }]
    });
  } else {
    template.push({
      label: 'File',
      submenu: [{
        label: 'Exit',
        accelerator: 'alt+f4',
        click: stopAllProcess
      }]
    });
  }
  template.push([{
    label: 'Edit',
    submenu: [{
      label: 'Undo',
      accelerator: 'CmdOrCtrl+Z',
      selector: 'undo:'
    }, {
      label: 'Redo',
      accelerator: 'Shift+CmdOrCtrl+Z',
      selector: 'redo:'
    }, {
      type: 'separator'
    }, {
      label: 'Cut',
      accelerator: 'CmdOrCtrl+X',
      selector: 'cut:'
    }, {
      label: 'Copy',
      accelerator: 'CmdOrCtrl+C',
      selector: 'copy:'
    }, {
      label: 'Paste',
      accelerator: 'CmdOrCtrl+V',
      selector: 'paste:'
    }, {
      label: 'Select All',
      accelerator: 'CmdOrCtrl+A',
      selector: 'selectAll:'
    }]
  }]);

  if (config.isDevEnv){
    var devTemplate = {
      label: 'Dev Tools',
      submenu: [{
        label: 'Enable DevTools',
        accelerator: 'Shift+CmdOrCtrl+I',
        click: enableDevTools
      },{
        label: "Download Logs",
        click: utils.downloadLogs
      }]
    };
    template.push(devTemplate);
  }
  log.debug("Setting menu options. "+JSON.stringify(template));
  var menu = Menu.buildFromTemplate(template);
  Menu.setApplicationMenu(menu);
}

/**
* Set Tray Icon and menu.
*/
function setTray(){
  log.debug("Setting Tray Icon");
  var tray = new Tray(config.trayImage);
  const contentMenu = Menu.buildFromTemplate([
    {label:"Open Appvance Window", click: startUpWindow},
    {label:"Shutdown Appvance", click: stopAllProcess}
  ]);
  tray.setToolTip('Start/Stop Appvance');
  tray.setContextMenu(contentMenu);
}

/**
* Startup window called after setup page. 
*/
function startUpWindow(){
  app.dock.show();
  // Create the browser window.
  var options = {
    width: 1400,
    height: 1000,
    show: false
  };

  var startUpBrowserWindow = new BrowserWindow(options);
  global.windows[0] = startUpBrowserWindow;
  if (config.isDevEnv){
    startUpBrowserWindow.openDevTools();    
  }

  if (config.mysqlType === "default"){
    installMysql(function(err){
      if(err){
        log.error("Failed to install mysql");
        log.error(err);
      } else{
        startMysql();
        maintainDbConnection();
        provisionMysql(function(err){
          if (err){
            log.error("Failed to Provision Mysql");
            log.error(err);
          } else{
                     
          }
        });
      }
    });
  } else if(config.mysqlType === "cred"){
    maintainDbConnection();
    provisionMysql(function(err){
      if(err){
        log.error("Failed to Provision mysql");
        log.error(err);
      }
    });
  } 
  //else user provisions mysql on their own and we are trusting it is up and running


  startAppvanceWindow();
  log.info("Starting Appvance Electron");

  if (config.registered){
    startUpBrowserWindow.loadURL(`file://${__dirname}/init.html`);
  } else {
    startUpBrowserWindow.loadURL(`file://${__dirname}/signin.html`);
  }

  startUpBrowserWindow.on('ready-to-show', function() {
    startUpBrowserWindow.show();
    startUpBrowserWindow.focus();
    global.windows[0] = startUpBrowserWindow;
    startUpBrowserWindow = null;
  });




  // Emitted when the window is closed.
  startUpBrowserWindow.on('closed', function() {
    app.dock.hide();
    startUpBrowserWindow = null;
  });

  // This is handle someone providing credentials to verify their appvance instance.
  // and to grab their license to set on appvance but currently, this will be skipped 
  // for now.
  electron.ipcMain.on('register', (event, arg) => {
    acmConnector.registerClient(arg.companyName, arg.userName, (err, license) => {
      if (err) {
        log.error("Failure registering with acm");
        var message={
          success: false
        };
        if (err.code === "ENOTFOUND"){
          message.information = "Unable to resolve '"+err.hostname+"' Are you connected to the internet?";
        }
        startUpBrowserWindow.webContents.send('registerResult', message);
      } else {

        acmConnector.setLicense(license, function(err){
          if(err){
            log.error("Unable to set license");
            log.error(err);
            startUpBrowserWindow.webContents.send('registerResult', {
              success: false
            });
          } else{
            log.debug("Sending register message to client");
            startUpBrowserWindow.webContents.send('registerResult', {
              success: true
            });
            config.userName = arg.userName;
            config.companyName = arg.companyName;
            config.registered = true;
            config.saveSystemSettings();
          }
        });
      }
    });
  });
}

/**
* This loads the setup window to pick default or Advanced install.
*/
function setupWindow(){
  var options = {
    width: 1400,
    height: 1000
  };

  var setupBrowserWindow = new BrowserWindow(options);

  log.info("Starting Appvance Setup");
  setupBrowserWindow.loadURL(`file://${__dirname}/setup.html`);
  global.windows[0] = setupBrowserWindow;
  electron.ipcMain.on('install', function(event, arg){
    if (arg == "default"){
      startUpWindow();
    } else if(arg == "advanced"){
      advancedWindow();
    }
  });
}

/**
* This is the advanced setup window. Not fully implemented yet.
*/
function advancedWindow(){
  var options = {
    width: 1400,
    height: 1000,
    show: false
  };

  var advancedBrowserWindow = new BrowserWindow(options);
  if (config.isDevEnv){
    advancedBrowserWindow.openDevTools();    
  }

  log.info("Starting Appvance Setup");
  advancedBrowserWindow.loadURL(`file://${__dirname}/setupAdvanced.html`);

  advancedBrowserWindow.on('ready-to-show', function() {
    advancedBrowserWindow.show();
    advancedBrowserWindow.focus();
    global.windows[0] = advancedBrowserWindow;
    advancedBrowserWindow = null;
  });
  electron.ipcMain.on('setConfig', function(event, arg){
    config.mysqlType = arg.mysqlConfig.mysqlType;
    if (config.mysqlType === 'cred'){
      config.mysql.user = arg.mysqlConfig.user;
      config.mysql.password = arg.mysqlConfig.pass;
      config.mysql.host = arg.mysqlConfig.host;
      config.mysql.port = arg.mysqlConfig.port;
    } else if (config.mysqlType === 'self'){
      config.mysql.host = arg.mysqlConfig.host;
      config.mysql.port = arg.mysqlConfig.port;      
    }

    config.appvance.host = arg.appvanceConfig.host;
    config.appvance.port = arg.appvanceConfig.port;

    if (arg.appvanceSettings.user !== 'appvance'){
      utils.onceAppvanceUp(function(err){
        if(err){
          log.error(err);
        } else{
          utpApi.admin.loggin(config.appvance.host, config.appvance.user, config.appvance.password, function(err) {
            if (err) {
              log.error(err);
            } else {
              utpApi.admin.addUser(config.appvance.host, arg.appvanceConfig.user, arg.appvanceConfig.pass, 'admin', function(err){
                if(err){
                  log.error(err);
                } else{
                  config.appvance.user = arg.appvanceConfig.user;
                  config.appvance.password = arg.appvanceConfig.pass;
                }
              });
            }
          });
        }
      });
    } else if (arg.appvanceSettings.pass !== 'appvance'){
      utils.onceAppvanceUp(function(err){
        if(err){
          log.error(err);
        } else{
          utpApi.admin.loggin(config.appvance.host, config.appvance.user, config.appvance.password, function(err) {
            if (err) {
              log.error(err);
            } else {
              utpApi.admin.changePassword(config.appvance.host, arg.appvanceConfig.pass, function(err){
                if(err){
                  log.error(err);
                } else{
                  config.appvance.password = arg.appvanceConfig.password;
                }
              });
            }
          });
        }
      });
    }
    startUpWindow();
  });
}


function mainApplication(){
  setMenu();
  setTray();
  if (!config.mysqlInitialized){
    setupWindow();
  } else{
    startUpWindow();
  }
}


// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', mainApplication);

/**
* This will kill the application, ensure first that mysql and appvance are killed first.
*/
function stopAllProcess() {
  stoppingProgram = true;
  if (mysqlChild) {
    log.debug("stopping mysql");
    mysqlChild.kill();

  }
  if (appvanceChild) {
    log.debug("stopping appvance service");
    appvanceChild.kill();

  }

  var checkKilled = function(){
    setTimeout(function(){
      if(appvanceChild.pid){
        log.error("Still haven't killed appvance");
        checkKilled();
      } else{
        log.error("Appvance killed");
        config.appvanceCommandStderrWriteSteam.end();
        config.appvanceCommandStdoutWriteSteam.end();
      }
      if(mysqlChild.pid){
        log.error("Still haven't killed mysql");
        checkKilled();
      } else{
        log.error("Mysql killed");
        config.mysqlCommandStdoutWriteSteam.end();
        config.mysqlCommandStderrWriteSteam.end();
      }
      app.quit();
    }, 5000);
  };
}

app.on('will-quit', function() {
  stopAllProcess();
});



// Could make a global exception handler to keep appvance running but didn't 
// want to implement to implement this until this was "production" ready.

// process.on('uncaughtException', function (error) {
//   log.error("Handling uncaught error")
//   if (global.windows[0]){
//     global.windows[0].webContents.send('current-proc', 'error setting up Appvance.')
//   } else{
//     console.log(error)
//   }
//   log.error(error)
//     // Handle the error
// })
